from flask import Blueprint

bp = Blueprint('projets', __name__)

from app.projets import routes
