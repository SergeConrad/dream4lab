
from flask import Flask, Blueprint, current_app, g, session, request, url_for, redirect, \
    render_template, flash, abort
from flask_login import login_user, login_required, logout_user, current_user


from app.extensions import db as app_db
from   app.models.projet import Projet, Personne

from .forms import (
    ProjetNewForm, ProjetEditForm, ProjetDeleteForm,
)
from app.projets import bp


#friend
@bp.route('/projets/list', methods=['GET', 'POST'])
#@login_required
def projets_list():

    projets = app_db.session.query(Projet).order_by(Projet.name).all()

    thead_th_items = [
        {
            'col_title': '#',
        },
        {
            'col_title': 'Name',
        },
        {
            'col_title': 'Personnes',
        },
        {
            'col_title': 'Delete',
        },
    ]

    tbody_tr_items = []
    for projet in projets:
        projet_names = '-'
        if projet.personnes:
            personne_names = ', '.join([x.name for x in projet.personnes])
        else:
            personne_names = ''

        tbody_tr_items.append([
            {
                'col_value': projet.id,
            },
            {
                'col_value': projet.name,
                'url': url_for('projets.projet_edit', projet_id=projet.id),
            },
            {
                'col_value': personne_names,
            },
            {
                'col_value': 'delete',
                'url': url_for('projets.projet_delete', projet_id=projet.id),
            }
        ])

    return render_template(
        'personnels/items_list.html',
        title='Projets',
        thead_th_items=thead_th_items,
        tbody_tr_items=tbody_tr_items,
        item_new_url=url_for('projets.projet_new'),
        item_new_text='Nouveau Projet',
    )





#friend -> projet
# hobbies -> personnes
@bp.route('/projet/new', methods=['GET', 'POST'])
def projet_new():

    item = Projet()
    form = ProjetNewForm()
    form.personnes.query = app_db.session.query(Personne).order_by(Personne.name)

    if form.validate_on_submit():
        form.populate_obj(item)
        app_db.session.add(item)
        app_db.session.commit()
        flash('Projet added: ' + item.name, 'info')
        return redirect(url_for('projets.projets_list'))

    return render_template('personnels/item_new_edit.html', title='New Projet', form=form)

#friend -> projet
# hobbies -> personnes
@bp.route('/projet/edit/<int:projet_id>', methods=['GET', 'POST'])
def projet_edit(projet_id):

    item = app_db.session.query(Projet).filter(Projet.id == projet_id).first()
    if item is None:
        abort(403)

    form = ProjetEditForm(obj=item)
    form.personnes.query = app_db.session.query(Personne).order_by(Personne.name)

    if form.validate_on_submit():
        form.populate_obj(item)
        app_db.session.commit()
        flash('Projet updated: ' + item.name, 'info')
        return redirect(url_for('projets.projets_list'))

    return render_template('personnels/item_new_edit.html', title='Edit projet', form=form)

#friend -> projet
# hobbies -> personnes
@bp.route('/projet/delete/<int:projet_id>', methods=['GET', 'POST'])
def projet_delete(projet_id):

    item = app_db.session.query(Projet).filter(Projet.id == projet_id).first()
    if item is None:
        abort(403)

    form = ProjetDeleteForm(obj=item)

    item_name = item.name
    if form.validate_on_submit():
        app_db.session.delete(item)
        app_db.session.commit()
        flash('Deleted projet: ' + item_name, 'info')
        return redirect(url_for('projets.projets_list'))

    return render_template('personnels/item_delete.html', title='Delete projet', item_name=item_name, form=form)


