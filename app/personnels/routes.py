





from flask import Flask, Blueprint, current_app, g, session, request, url_for, redirect, \
    render_template, flash, abort
from flask_login import login_user, login_required, logout_user, current_user


from app.extensions import db as app_db
from   app.models.projet import Projet, Personne

from .forms import (
    PersonneNewForm, PersonneEditForm, PersonneDeleteForm,
)
from app.personnels import bp



#hobbies ->personnes
@bp.route('/personnes/list', methods=['GET', 'POST'])
def personnes_list():

    personnes = app_db.session.query(Personne).order_by(Personne.name).all()

    thead_th_items = [
        {
            'col_title': '#',
        },
        {
            'col_title': 'Name',
        },
        {
            'col_title': 'Prénom',
        },
        {
            'col_title': 'Projets',
        },
        {
            'col_title': 'Delete',
        },
    ]

    tbody_tr_items = []
    for personne in personnes:
        projet_names = ''
        if personne.projets:
            #projet_names = ', '.join([x.name for x in personnes.projets])
            projet_names = ', '.join([x.name for x in personne.projets])
        tbody_tr_items.append([
            {
                'col_value': personne.id,
            },
            {
                'col_value': personne.name,
                'url': url_for('personnels.personne_edit', personne_id=personne.id),
            },
            {
                'col_value': personne.prenom,
            },
            {
                'col_value': projet_names,
            },
            {
                'col_value': 'delete',
                'url': url_for('personnels.personne_delete', personne_id=personne.id),
            }
        ])

    return render_template(
        'personnels/items_list.html',
        title='Personnes',
        thead_th_items=thead_th_items,
        tbody_tr_items=tbody_tr_items,
        item_new_url=url_for('personnels.personne_new'),
        item_new_text='New Personne',
    )



#friend -> projet
# hobbies -> personnes

@bp.route('/personne/new', methods=['GET', 'POST'])
def personne_new():

    item = Personne()
    form = PersonneNewForm()

    if form.validate_on_submit():
        form.populate_obj(item)
        app_db.session.add(item)
        app_db.session.commit()
        flash('Personne added: ' + item.name, 'info')
        return redirect(url_for('personnels.personnes_list'))

    return render_template('personnels/item_new_edit.html', title='New Personne', form=form)

#friend -> projet
# hobbies -> personnes
@bp.route('/personne/edit/<int:personne_id>', methods=['GET', 'POST'])
def personne_edit(personne_id):

    item = app_db.session.query(Personne).filter(Personne.id == personne_id).first()
    if item is None:
        abort(403)

    form = PersonneEditForm(obj=item)

    if form.validate_on_submit():
        form.populate_obj(item)
        app_db.session.commit()
        flash('Personne updated: ' + item.name, 'info')
        return redirect(url_for('personnels.personnes_list'))

    return render_template('personnels/item_new_edit.html', title='Edit personne', form=form)

#friend -> projet
# hobbies -> personnes
@bp.route('/personne/delete/<int:personne_id>', methods=['GET', 'POST'])
def personne_delete(personne_id):

    item = app_db.session.query(Personne).filter(Personne.id == personne_id).first()
    if item is None:
        abort(403)

    form = PersonneDeleteForm(obj=item)

    item_name = item.name
    if form.validate_on_submit():
        app_db.session.delete(item)
        app_db.session.commit()
        flash('Deleted personne: ' + item_name, 'info')
        return redirect(url_for('personnels.personnes_list'))

    return render_template('personnels/item_delete.html', title='Delete personne', item_name=item_name, form=form)
