from flask import Flask
from flask_login import LoginManager
from flask_migrate import Migrate

from flask_bootstrap import Bootstrap5


from config import Config
from app.extensions import db

def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)

    # Initialize Flask extensions here
    db.init_app(app)
    migrate = Migrate(app, db)
    # Bootstrap-Flask requires this line
    bootstrap = Bootstrap5(app)

    # Flask-WTF requires this line
    #csrf = CSRFProtect(app)


    # Register blueprints here
    from app.main import bp as main_bp
    app.register_blueprint(main_bp)

    from app.personnels import bp as personnels_bp
    app.register_blueprint(personnels_bp, url_prefix='/personnels')

    from app.projets import bp as projets_bp
    app.register_blueprint(projets_bp, url_prefix='/projets')

    login_manager = LoginManager()
    login_manager.login_view = 'main.login'
    login_manager.init_app(app)

    from app.models.user import User
    from   app.models.projet import Projet, Personne

    @login_manager.user_loader
    def load_user(user_id):
        # since the user_id is just the primary key of our user table, use it in the query for the user
        return User.query.get(int(user_id))



    @app.route('/test/')
    def test_page():
        return '<h1>Testing the Flask Application Factory Pattern</h1>'

    return app
