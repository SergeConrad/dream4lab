import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    SECRET_KEY = 'your secret key'
    SQLALCHEMY_DATABASE_URI = 'mysql://app:password@localhost/projet7'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
