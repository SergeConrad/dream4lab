# forms.py

from flask_wtf import FlaskForm
from wtforms import IntegerField, StringField, SubmitField
from wtforms_sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField
from wtforms.validators import InputRequired, Length

from app.extensions import db as app_db
from   app.models.projet import Projet, Personne


# personne
class PersonneNewEditFormMixin():

    name = StringField('Name',
        validators=[ InputRequired(), Length(min=2) ])
    prenom = StringField('prenom',
        validators=[ InputRequired(), Length(min=2) ])

class PersonneNewForm(FlaskForm, PersonneNewEditFormMixin):

    submit = SubmitField('Add')

class PersonneEditForm(FlaskForm, PersonneNewEditFormMixin):

    submit = SubmitField('Update')

class PersonneDeleteForm(FlaskForm):

    submit = SubmitField('Confirm delete')

