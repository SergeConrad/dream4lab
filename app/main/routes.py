from flask import render_template ,request, session, redirect, url_for, flash
from flask import current_app as app
from flask_login import login_user, login_required, logout_user, current_user

from werkzeug.security import generate_password_hash, check_password_hash


from app.main import bp
#from app.models.comptes import Comptes
from app.models.user import User
from app.extensions import db
import re,hashlib
#ldap ?



@bp.route('/register')
def register():
    return render_template('register.html')

@bp.route('/register', methods=['POST'])
def register_post():
    # code to validate and add user to database goes here
    email = request.form.get('email')
    name = request.form.get('name')
    password = request.form.get('password')

    user = User.query.filter_by(email=email).first() # if this returns a user, then the email already exists in database

    if user: # if a user is found, we want to redirect back to signup page so user can try again
        flash('Email address already exists')
        return redirect(url_for('main.register'))

    # create a new user with the form data. Hash the password so the plaintext version isn't saved.
    new_user = User(email=email, name=name, password=generate_password_hash(password, method='pbkdf2:sha256'))

    # add the new user to the database
    db.session.add(new_user)
    db.session.commit()

    return redirect(url_for('main.login'))




@bp.route('/')
def login():
    return render_template('login.html')

@bp.route('/', methods=['POST'])
def login_post():
    # login code goes here
    email = request.form.get('email')
    password = request.form.get('password')
    remember = True if request.form.get('remember') else False

    #account = Comptes.query.filter_by(username=username, password=password).first()

    user = User.query.filter_by(email=email).first()

    # check if the user actually exists
    # take the user-supplied password, hash it, and compare it to the hashed password in the database
    if not user or not check_password_hash(user.password, password):
        flash('Please check your login details and try again.')
        return redirect(url_for('main.login')) # if the user doesn't exist or password is wrong, reload the page

    # if the above check passes, then we know the user has the right credentials
    login_user(user, remember=remember)
    return redirect(url_for('main.home'))


#def adlogin():
#    # Output message if something goes wrong...
#    msg = ''
#
#    LDAP_SERVER = "fleur.hsm.local"
#    LDAP_PORT = 389 # your port
#    ld = ldap.open(LDAP_SERVER, port=LDAP_PORT)

#    return render_template('adlogin.html', msg=msg)

#    # Check if "username" and "password" POST requests exist (user submitted form)
#    if request.method == 'POST' and 'username' in request.form and 'password' in request.form:
#        # Create variables for easy access
#        username = request.form['username']
#        password = request.form['password']
#
#        try:
#            ld.simple_bind_s(username, password)
#            session['loggedin'] = True
#            session['id'] = 1 # ?
#            session['username'] = "?"
#            # Account exists
#            return redirect(url_for('main.home'))
#        except ldap.INVALID_CREDENTIALS:
#            msg = 'Incorrect username/password!'
#            return False
#    return render_template('adlogin.html', msg=msg)


def generate_page_list():
    pages = [
        {"name": "Details", "url": url_for("main.home")},
        {"name": "Events", "url": url_for("main.home")},
        {"name": "Environment", "url": url_for("main.home")},
    ]
    return pages

@bp.route('/home')
@login_required
def home():
    pages=generate_page_list()
    return render_template('home.html', username=current_user.name,pages=pages)



## VERSION flask_login
@bp.route('/profile')
@login_required
def profile():
        return render_template('profile.html', name=current_user.name)

@bp.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('main.login'))



