from app.extensions import db




class Projet(db.Model):
    __tablename__ = 'projets'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))  # Specify the length for VARCHAR

    personnes = db.relationship('Personne', secondary='projet_personne_association', back_populates='projets')

class Personne(db.Model):
    __tablename__ = 'personnes'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))  # Specify the length for VARCHAR
    prenom = db.Column(db.String(255))  # Specify the length for VARCHAR

    projets = db.relationship('Projet', secondary='projet_personne_association', back_populates='personnes')

projet_personne_association = db.Table(
    'projet_personne_association',
    db.Column('projet_id', db.Integer, db.ForeignKey('projets.id')),
    db.Column('personne_id', db.Integer, db.ForeignKey('personnes.id'))
)

