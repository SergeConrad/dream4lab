# forms.py

from flask_wtf import FlaskForm
from wtforms import IntegerField, StringField, SubmitField
from wtforms_sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField
from wtforms.validators import InputRequired, Length

from app.extensions import db as app_db
from   app.models.projet import Projet, Personne

class ProjetNewEditFormMixin():

    name = StringField('Name',
        validators=[ InputRequired(), Length(min=4) ])


    personnes = QuerySelectMultipleField('Personnes',
        get_label='name',
        allow_blank=False,
        blank_text='Select one or more personnes',
        render_kw={'size': 10},
        )

class ProjetNewForm(FlaskForm, ProjetNewEditFormMixin):

    submit = SubmitField('Add')

class ProjetEditForm(FlaskForm, ProjetNewEditFormMixin):

    submit = SubmitField('Update')

class ProjetDeleteForm(FlaskForm):

    submit = SubmitField('Confirm delete')


